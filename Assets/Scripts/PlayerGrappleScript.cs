using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GrappleDirection
{
    Backwards,
    Forwards,
    Left,
    Right
}

public class PlayerGrappleScript : MonoBehaviour
{

    // prefabs for left and right grapple chord objects
    public GameObject leftGrappleChordPrefab =null;
    public GameObject rightGrappleChordPrefab = null;
    // speed of grapple chord extension
    public float extensionSpeed = 1.0f;
    // vertical component of force on player from grapple
    public float verticalForce = 1.0f;
    // horizontal component of force on player from grapple
    public float horizontalForce = 1.0f;
    // torque which rotates player to align to face object when grappling
    public float torque = 1.0f;
    // max speed player can move when grappling.
    public float speedLimit = 5.0f;

    // pobjects connected to grapples
    private GrappleableObjectScript leftSelectedGrappleable = null;
    private GrappleableObjectScript rightSelectedGrappleable = null;

    private bool leftSecured = false; // flag for when grapple connected to object
    private GrappleChordScript leftGrapple = null;

    private bool rightSecured = false; // flag for when grapple connected to object
    private GrappleChordScript rightGrapple = null;

    // Start is called before the first frame update
    void Start()
    {
        // event subscriptions
        EventManagerScript.OnGrappleChoose += SelectGrappleable;
        EventManagerScript.OnGrappleActivated += ActivateGrapple;
        EventManagerScript.OnGrappleSecured += SecureGrapple;
        EventManagerScript.OnGrappleReleased += ReleaseGrapple;

    }

    // Update is called once per frame
    void Update()
    {
        var rigidBody = transform.GetComponentInParent<Rigidbody>();

        // if a hand is secured to a grappleable object, apply grapple force
        if (leftSecured)
        {
            ApplyGrappleForce(rigidBody, leftGrapple, GrappleDirection.Left);
        }
        if (rightSecured)
        {
            ApplyGrappleForce(rigidBody, rightGrapple, GrappleDirection.Right);
        }

    }

    private void ApplyGrappleForce(Rigidbody rigidBody, GrappleChordScript chordScript, GrappleDirection hand)
    {
        if (chordScript != null)
        {
            // get vector pointing towards grapple location
            Vector3 displacement = chordScript.target.transform.position - transform.position;
            // get direction of vec
            Vector3 direction = displacement.normalized;

            // get distance to target
            float distance = displacement.magnitude;
            rigidBody.AddForce(new Vector3(direction.x * horizontalForce, direction.y * verticalForce, direction.z * horizontalForce));

            // rotate player towards grapple point
            float rotationAngle = Vector3.SignedAngle(direction, transform.parent.transform.forward, Vector3.down);
            rigidBody.AddTorque(new Vector3(0, torque * rotationAngle, 0));
        }
    }

    //------------------------------------------------
    // Grapple toggle and activation methods
    //------------------------------------------------
    private void DeselectGrappleable(GrappleDirection direction)
    {
        if(direction == GrappleDirection.Left && leftSelectedGrappleable != null)// left grapple
        {
            // deactivate grapple UI, clear stored grappleable reference
            leftSelectedGrappleable.DeactivateGrappleUI(GrappleDirection.Left);
            leftSelectedGrappleable = null;
        }
        else if(rightSelectedGrappleable != null) // right grapple
        {
            // deactivate grapple UI, clear stored grappleable reference
            rightSelectedGrappleable.DeactivateGrappleUI(GrappleDirection.Right);
            rightSelectedGrappleable = null;
        }
    }
    // Select grappleable, i.e. enable firing the grapple at this object
    private void SelectGrappleable(GrappleableObjectScript grappleable)
    {
        // if the selected grappleable is not already selected (stored), select (store)
        // NOTE that both hands can share the same grappleable

        if(leftGrapple == null && grappleable != leftSelectedGrappleable) // left hand
        {
            // Deactivate old UI and activate new UI, store grappleable
            if (leftSelectedGrappleable != null)
            {
                leftSelectedGrappleable.DeactivateGrappleUI(GrappleDirection.Left);
            }
            grappleable.ActivateGrappleUI(GrappleDirection.Left);
            leftSelectedGrappleable = grappleable;
        }

        if (rightGrapple == null && grappleable != rightSelectedGrappleable) // right hand
        {
            // Deactivate old UI and activate new UI, store grappleable
            if (rightSelectedGrappleable != null)
            {
                rightSelectedGrappleable.DeactivateGrappleUI(GrappleDirection.Right);
            }
            grappleable.ActivateGrappleUI(GrappleDirection.Right);
            rightSelectedGrappleable = grappleable;
        }
    }
    // Activate the grapple, i.e. fire towards grappleable
    private void ActivateGrapple(GrappleDirection direction)
    {
        if(direction == GrappleDirection.Left && leftSelectedGrappleable != null) // left hand
        {
            if (leftGrapple == null)
            {
                // instantiate and store grapple chord object from prefab
                leftGrapple = Object.Instantiate(leftGrappleChordPrefab, transform).GetComponent<GrappleChordScript>();
                // set up chord parameters
                leftGrapple.grappleHand = GrappleDirection.Left;
                leftGrapple.speed = extensionSpeed;
                leftGrapple.target = leftSelectedGrappleable;
                leftGrapple.playerGrapple = this;
            }
        }
        else if(rightSelectedGrappleable != null) // right hand
        {
            if (rightGrapple == null)
            {
                // instantiate and store grapple chord object from prefab
                rightGrapple = Object.Instantiate(rightGrappleChordPrefab, transform).GetComponent<GrappleChordScript>();
                // set up chord parameters
                rightGrapple.grappleHand = GrappleDirection.Right;
                rightGrapple.speed = extensionSpeed;
                rightGrapple.target = rightSelectedGrappleable;
                rightGrapple.playerGrapple = this;
            }
        }
    }
    // Grapple has reached target, secure, i.e. start applying forces
    private void SecureGrapple(GrappleDirection direction)
    {
        // deselect the grapplable (turns off UI)
        DeselectGrappleable(direction);
        // flag as secured, will now apply forces in update
        if (direction == GrappleDirection.Left)
        {
            leftSecured = true;
        }
        else
        {
            rightSecured = true;
        }
    }
    private void ReleaseGrapple(GrappleDirection direction)
    {
        // deselect the grapplable (turns off UI)
        DeselectGrappleable(direction);

        if (direction == GrappleDirection.Left)
        {
            leftSecured = false; // unflag as secured, will now stop applying forces
            if (leftGrapple)
            {
                // destroy grapple chord
                Destroy(leftGrapple.gameObject);
            }
            leftGrapple = null;
        }
        else
        {
            rightSecured = false; // unflag as secured, will now stop applying forces
            if (rightGrapple)
            {
                // destroy grapple chord
                Destroy(rightGrapple.gameObject);
            }
            rightGrapple = null;
        }
    }
}
