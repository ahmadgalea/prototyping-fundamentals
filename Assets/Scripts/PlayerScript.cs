using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public float maxHealth = 100;

    public float enemyHitDamage = 20;
    public float bulletHitDamage = 10;

    public float gravity = -5;

    public HealthMeterScript healthBar;

    private bool gamePaused = true;

    private float health = 100;

    private Rigidbody rigidBody;

    // Start is called before the first frame update
    void Start()
    {
        EventManagerScript.OnGameStateChange += PauseUnpausePlayer;

        Reset();
    }

    private void Reset()
    {
        rigidBody = gameObject.GetComponent<Rigidbody>();

        if (maxHealth <= 0)
        {
            maxHealth = 1;
        }
        health = maxHealth;
        healthBar.UpdateHealth(health, maxHealth);
        transform.position = new Vector3(0, 120, -46);
        transform.LookAt(new Vector3(0, 120, 0));
    }

    // Update is called once per frame
    void Update()
    {
        if (gamePaused)
        {
            return;
        }
       
        rigidBody.AddForce(rigidBody.mass * (new Vector3(0, gravity, 0)));
    }

    private void TakeDamage(float damage)
    {
        health -= damage;

        if(health <= 0)
        {
            health = 0;
            EventManagerScript.ChangeGameState(GameState.Paused);
            EventManagerScript.ChangeUIState(UIState.GameOverMenu);
        }
        if(healthBar)
        {
            healthBar.UpdateHealth(health, maxHealth);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Enemy")
        {
            TakeDamage(enemyHitDamage);

            // bounce off
            rigidBody.velocity += 4.0f*collision.impulse;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bullet")
        {
            TakeDamage(bulletHitDamage);
            Destroy(other.gameObject);
        }
    }

        private void PauseUnpausePlayer(GameState newState)
    {
        if (newState == GameState.Paused)
        {
            gamePaused = true;
            rigidBody.velocity = Vector3.zero;
        }
        else if (newState == GameState.Playing)
        {
            gamePaused = false;
        }
        else if(newState == GameState.Restart)
        {
            gamePaused = false;
            rigidBody.velocity = Vector3.zero;
            Reset();
        }
    }
}
