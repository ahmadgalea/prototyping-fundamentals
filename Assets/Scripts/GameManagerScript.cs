using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    Menu,
    Playing,
    Paused,
    Quit,
    Restart
}
public class GameManagerScript : MonoBehaviour
{
    Resolution res;

    public static GameState gameState = GameState.Menu;

    // Start is called before the first frame update
    void Start()
    {

        EventManagerScript.OnGameStateChange += ChangeGameState;

        res = Screen.currentResolution;
        if (res.refreshRate == 60)
            QualitySettings.vSyncCount = 1;
        if (res.refreshRate == 120)
            QualitySettings.vSyncCount = 2;

        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeGameState(GameState newState)
    {
        gameState = newState;
        switch(newState)
        {
            case GameState.Menu:
                break;
            case GameState.Playing:
                //Time.timeScale = 1;
                break;
            case GameState.Paused:
                //Time.timeScale = 0;
                break;
            case GameState.Restart:
                break;
            case GameState.Quit:
                Application.Quit();
                break;
            default:
                break;
        }
    }
}
