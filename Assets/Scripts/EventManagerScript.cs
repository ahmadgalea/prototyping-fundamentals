using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EventManagerScript : MonoBehaviour
{

    public delegate void GameStateChange(GameState state);
    public static event GameStateChange OnGameStateChange;

    public delegate void UIStateChange(UIState state);
    public static event UIStateChange OnUIStateChange;

    public delegate void MouseMovedEvents(Vector3 mouseDelta);
    public static event MouseMovedEvents OnMouseMoved;

    public delegate void GrappleAction(GrappleDirection direction);
    public static event GrappleAction OnGrappleSwitch;
    public static event GrappleAction OnGrappleActivated;
    public static event GrappleAction OnGrappleSecured;
    public static event GrappleAction OnGrappleReleased;

    public delegate void GrappleChoose(GrappleableObjectScript grappleable);
    public static event GrappleChoose OnGrappleChoose;

    public delegate void WallJumpEnableAction(GrappleableObjectScript grappleable);
    public static event WallJumpEnableAction OnWallJumpEnabled;

    public delegate void WallJumpAction();
    public static event WallJumpAction OnWallJumpActivated;
    public static event WallJumpAction OnWallJumpFired;
    public static event WallJumpAction OnWallJumpDisabled;



    public static void ChangeGameState(int newState)
    {
        ChangeGameState((GameState)newState);
    }
    public static void ChangeGameState(GameState newState)
    {
        if(EventManagerScript.OnGameStateChange != null)
        {
            EventManagerScript.OnGameStateChange(newState);
        }
    }

    public static void ChangeUIState(int newState)
    {
        ChangeUIState((UIState)newState);
    }

    public static void ChangeUIState(UIState newState)
    {
        if (EventManagerScript.OnUIStateChange != null)
        {
            EventManagerScript.OnUIStateChange(newState);
        }
    }

    public static void MouseMoved(Vector3 mouseDelta)
    {
        if(EventManagerScript.OnMouseMoved != null)
        {
            EventManagerScript.OnMouseMoved(mouseDelta);
        }
    }
    public static void SwitchGrapple(GrappleDirection direction)
    {
        if (EventManagerScript.OnGrappleSwitch != null)
        {
            EventManagerScript.OnGrappleSwitch(direction);
        }
    }

    public static void ActivateGrapple(GrappleDirection direction)
    {
        if (EventManagerScript.OnGrappleActivated != null)
        {
            EventManagerScript.OnGrappleActivated(direction);
        }
    }

    public static void SecureGrapple(GrappleDirection direction)
    {
        if (EventManagerScript.OnGrappleSecured != null)
        {
            EventManagerScript.OnGrappleSecured(direction);
        }
    }

    public static void ReleaseGrapple(GrappleDirection direction)
    {
        if (EventManagerScript.OnGrappleReleased != null)
        {
            EventManagerScript.OnGrappleReleased(direction);
        }
    }

    public static void ChooseGrapple(GrappleableObjectScript grappleable)
    {
        if (EventManagerScript.OnGrappleChoose != null)
        {
            EventManagerScript.OnGrappleChoose(grappleable);
        }
    }


    public static void EnableWallJump(GrappleableObjectScript grappleable)
    {
        if (EventManagerScript.OnWallJumpEnabled != null)
        {
            EventManagerScript.OnWallJumpEnabled(grappleable);
        }
    }

    public static void DisableWallJump()
    {
        if (EventManagerScript.OnWallJumpDisabled != null)
        {
            EventManagerScript.OnWallJumpDisabled();
        }
    }

    public static void ActivateWallJump()
    {
        if (EventManagerScript.OnWallJumpActivated != null)
        {
            EventManagerScript.OnWallJumpActivated();
        }
    }

    public static void FireWallJump()
    {
        if (EventManagerScript.OnWallJumpFired != null)
        {
            EventManagerScript.OnWallJumpFired();
        }
    }

}
