using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossScript : MonoBehaviour
{

    public float maxHealth = 400;
    public float objectHitDamage = 40;

    public int shotsPerSecond = 3;
    public int patternInterval = 4;
    public int patternGapInterval = 2;
    public int gapFrequency = 4;

    public float bulletSpeed = 4;

    public float bossRotationSpeed = 2.0f;

    public HealthMeterScript healthBar;

    public GameObject bulletPrefab;

    private float health = 400;

    private float shotTimer = 0.0f;
    private float intervalTimer = 0.0f;
    private float pauseTimer = 0.0f;
    private int intervalCounter = 0;
    private bool onBreak = true;

    private bool gamePaused = true;
    private float cumulativeAngle = 0.0f;

    private GameObject bulletContainer = null;

    // Start is called before the first frame update
    void Start()
    {
        Reset();
    }

    private void Reset()
    {
        if (maxHealth <= 0)
        {
            maxHealth = 1;
        }
        health = maxHealth;
        healthBar.UpdateHealth(health, maxHealth);

        if(bulletContainer)
        {
            Destroy(bulletContainer.gameObject);
        }

        bulletContainer = new GameObject();
        Object.Instantiate(bulletContainer, transform);

        EventManagerScript.OnGameStateChange += PauseUnpauseBoss;
    }

    // Update is called once per frame
    void Update()
    {
        if(gamePaused)
        {
            return;
        }

        if(!onBreak)
        {
            shotTimer += Time.deltaTime;
            if (shotTimer >= 1.0f / shotsPerSecond)
            {
                TakeShot();
                shotTimer = 0;
            }

            intervalTimer += Time.deltaTime;
            if (intervalTimer >= patternInterval)
            {
                intervalTimer = 0.0f;
                intervalCounter++;
                if (intervalCounter == gapFrequency)
                {
                    onBreak = true;
                    intervalCounter = 0;
                }
            }
        }
        else
        {
            pauseTimer += Time.deltaTime;
            if(pauseTimer >= patternGapInterval)
            {
                pauseTimer = 0.0f;
                onBreak = false;
            }
        }

    }

    private void TakeShot()
    {
        var bullet = Object.Instantiate(bulletPrefab, transform.position, new Quaternion());
        bullet.transform.parent = bulletContainer.transform;

        float yVelocity = bulletSpeed*(2.0f * (intervalTimer / patternInterval) - 1.0f);

        float frac = intervalTimer / patternInterval;
        float angle = Mathf.PI * (2*frac - 1) + cumulativeAngle;

        cumulativeAngle += bossRotationSpeed;
        float xVelocity = bulletSpeed * Mathf.Cos(angle);
        float zVelocity = bulletSpeed * Mathf.Sin(angle);
        Vector3 bulletVelocity = new Vector3(xVelocity, yVelocity, zVelocity);
        bullet.GetComponent<Rigidbody>().velocity = bulletVelocity;
    }

    private void TakeDamage(float damage)
    {
        health -= damage;

        if (health <= 0)
        {
            health = 0;
            EventManagerScript.ChangeGameState(GameState.Paused);
            EventManagerScript.ChangeUIState(UIState.VictoryMenu);
        }
        if (healthBar)
        {
            healthBar.UpdateHealth(health, maxHealth);
        }
    }

    private void PauseUnpauseBoss(GameState newState)
    {
        if (newState == GameState.Paused)
        {
            gamePaused = true;
        }
        else if (newState == GameState.Playing)
        {
            gamePaused = false;
        }
        else if (newState == GameState.Restart)
        {
            gamePaused = false;
            Reset();
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag != "Player")
        {
            TakeDamage(objectHitDamage);
            Destroy(collision.gameObject);
        }
    }
}
