using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWallJumpScript : MonoBehaviour
{
    // speed of auto camera adjustment
    public float snapRotateSpeed = 2.0f;
    // free rotate camera speed when wall jump being aimed
    public float cameraFreeRotateSpeed = 1.0f;
    // wall jump force vale
    public float forceValue = 30.0f;

    private GrappleableObjectScript currentJumpingObject = null;
    private bool wallJumpActive = false;
    private bool cameraAdjusting = false;

    // Start is called before the first frame update
    void Start()
    {
        // subscribe to events
        EventManagerScript.OnWallJumpEnabled += EnableWallJump;
        EventManagerScript.OnWallJumpDisabled += DisableWallJump;
        EventManagerScript.OnWallJumpActivated += ActivateWallJump;
        EventManagerScript.OnMouseMoved += MoveMouseCamera;
    }

    void Update()
    {
        // if the camera is flagged as 'updating',
        // we are in the mode where the playera dn camera are automatically 
        // adjusting so that the player is facing the object,
        // this happens when the wall jump is activated, either initailly or when fired.
        var rigidBody = transform.GetComponentInParent<Rigidbody>();
        if(cameraAdjusting)
        {
            AdjustCamera(rigidBody);
        }
    }

    private void AdjustCamera(Rigidbody rigidBody)
    {
        // get rotation amount
        float delta = snapRotateSpeed * Time.deltaTime;
        Vector3 direction;

        if (wallJumpActive && currentJumpingObject) // wall jump first activated
        {
            // align player with object
            var displacement = currentJumpingObject.transform.position - transform.parent.position;
            direction = displacement.normalized;
        }
        else // wall jump has been fired
        {
            // allign in opposite direction to x-z velocity.
            direction = -rigidBody.velocity.normalized;
            direction.y = 0;
            if(direction.normalized.magnitude == 0 )
            {
                direction.x = 1.0f;
            }

        }

        // while player is not aligned, rotate player
        if (Vector3.Dot(transform.parent.forward.normalized,direction.normalized) < 0.99)
        {
            var newDirection = Vector3.RotateTowards(transform.parent.transform.forward, direction, delta, 0.0f);
            transform.parent.rotation = Quaternion.LookRotation(newDirection);
        }
        else
        {
            cameraAdjusting = false;
        }
    }

    // On mouse moved event
    private void MoveMouseCamera(Vector3 mouseDelta)
    {
        // if the wall jump is active and the camera is done its auto adjustment
        if(wallJumpActive && ! cameraAdjusting && currentJumpingObject)
        {
            // free rotate camera/player about wall juming object

            //x-z rotation (about y-axis)
            transform.parent.RotateAround(currentJumpingObject.transform.position, Vector3.up, mouseDelta.x * cameraFreeRotateSpeed);
            // y rotation (about players x-axis)
            Vector3 rotationAxis = transform.parent.TransformDirection(-1.0f, 0.0f, 0.0f);
            transform.parent.RotateAround(currentJumpingObject.transform.position, rotationAxis, mouseDelta.y * cameraFreeRotateSpeed);
            // make player face object
            transform.parent.LookAt(currentJumpingObject.transform.position);
        }
    }
    // on wall jump enabled (player is close to object)
    private void EnableWallJump(GrappleableObjectScript grappleable)
    {
        if (grappleable != null && currentJumpingObject == null)
        {
            // store object and activate UI
            currentJumpingObject = grappleable;
            grappleable.ActivateWallJumpUI();
        }
    }
    // on wall jump disabled (player is cfar from object)
    private void DisableWallJump()
    {
        if (currentJumpingObject != null)
        {
            // cancel wall jump, clear object reference and disable UI
            currentJumpingObject.DectivateWallJumpUI();
            currentJumpingObject = null;
            wallJumpActive = false;
        }
    }
    // called when wall jump is initiated or fired
    private void ActivateWallJump()
    {
        if(currentJumpingObject != null)
         {
            if (!wallJumpActive) // when wall jump initally activated
            {
                // flag active and pause game state
                wallJumpActive = true;
                EventManagerScript.ChangeGameState(GameState.Paused);
                cameraAdjusting = true;
                // realease both grapples
                EventManagerScript.ReleaseGrapple(GrappleDirection.Left);
                EventManagerScript.ReleaseGrapple(GrappleDirection.Right);
            }
            else // when wall jump fired
            {
                // flag inactive and play game state
                wallJumpActive = false;
                cameraAdjusting = true;
                EventManagerScript.ChangeGameState(GameState.Playing);
                // deactivate UI
                currentJumpingObject.DectivateWallJumpUI();

                // apply wall jump force to player and object
                ApplyForce();
            }
        }
    }

    private void ApplyForce()
    {
        // get both rigid bodies
        var objectBody = currentJumpingObject.GetComponent<Rigidbody>();
        var thisBody = GetComponentInParent<Rigidbody>();

        // shoot object in forwards direction and turn on gravity
        var direction = transform.parent.forward.normalized;
        objectBody.AddForce(forceValue * direction);
        objectBody.isKinematic = false;
        objectBody.useGravity = true;
        currentJumpingObject.GetComponent<BoxCollider>().isTrigger = false;
        // shoot player in opposite direction
        thisBody.AddForce(-forceValue * direction);
    }
}
