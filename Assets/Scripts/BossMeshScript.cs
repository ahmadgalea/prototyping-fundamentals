using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossMeshScript : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        transform.parent.gameObject.GetComponent<BossScript>().OnCollisionEnter(collision);
    }
}
