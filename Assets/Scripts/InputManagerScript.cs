using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManagerScript : MonoBehaviour
{

    public LayerMask layerToHit;

    private Vector3 lastMousePosition = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            if (UIManagerScript.uiState == UIState.InGame )
            {
                EventManagerScript.ChangeUIState(UIState.PauseMenu);
                EventManagerScript.ChangeGameState(GameState.Paused);
            }
            else if(UIManagerScript.uiState == UIState.PauseMenu)
            {
                EventManagerScript.ChangeUIState(UIState.InGame);
                EventManagerScript.ChangeGameState(GameState.Playing);
            }
        }

        if (Input.GetKeyDown("z"))
        {
            EventManagerScript.SwitchGrapple(GrappleDirection.Backwards);
        }
        if (Input.GetKeyDown("x"))
        {
            EventManagerScript.SwitchGrapple(GrappleDirection.Forwards);
        }

        if (Input.GetKeyDown("q") || Input.GetMouseButtonDown(0))
        {
            EventManagerScript.ActivateGrapple(GrappleDirection.Left);
        }
        if(Input.GetKeyUp("q") || Input.GetMouseButtonUp(0))
        {
            EventManagerScript.ReleaseGrapple(GrappleDirection.Left);
        }
        if (Input.GetKeyDown("e") || Input.GetMouseButtonDown(1))
        {
            EventManagerScript.ActivateGrapple(GrappleDirection.Right);
        }
        if(Input.GetKeyUp("e") || Input.GetMouseButtonUp(1))
        {
            EventManagerScript.ReleaseGrapple(GrappleDirection.Right);
        }

        if (GameManagerScript.gameState != GameState.Paused)
        {
            Ray cursorRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitObject;
            if (Physics.Raycast(cursorRay, out hitObject))
            {
                GrappleableObjectScript grappleable = hitObject.transform.gameObject.GetComponentInParent<GrappleableObjectScript>();
                if (grappleable)
                {
                    EventManagerScript.ChooseGrapple(grappleable);
                }
            }
        }

        if (Input.GetKeyDown("space"))
        {
            EventManagerScript.ActivateWallJump();
        }

        Vector3 mouseDelta = Input.mousePosition - lastMousePosition;
        lastMousePosition = Input.mousePosition;
        if(mouseDelta.magnitude > 0.0f)
        {
            EventManagerScript.MouseMoved(mouseDelta);
        }
    }
}
