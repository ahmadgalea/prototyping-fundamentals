using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleableObjectScript : MonoBehaviour
{

    private bool leftUIActive = false;
    private bool rightUIActive = false;
    private bool wallJumpActive = false;

    private GameObject canvas;
    private Camera camera;


    // Start is called before the first frame update
    void Start()
    {
        canvas = gameObject.transform.GetChild(0).gameObject;
        camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if(canvas.activeSelf)
        {
            canvas.transform.forward = camera.transform.forward;
        }
    }

    public void ActivateGrappleUI(GrappleDirection hand)
    {
        if (canvas != null)
        {
            canvas.SetActive(true);
            if (hand == GrappleDirection.Left)
            {
                GameObject grappleUI = canvas.transform.GetChild(0).gameObject;
                grappleUI.SetActive(true);
                leftUIActive = true;
            }
            else
            {
                GameObject grappleUI = canvas.transform.GetChild(1).gameObject;
                grappleUI.SetActive(true);
                rightUIActive = true;
            }
        }
    }

    public void DeactivateGrappleUI(GrappleDirection hand)
    {
        if (canvas != null)
        {
            if (!wallJumpActive)
            {
                canvas.SetActive(false);
            }
            if (hand == GrappleDirection.Left)
            {
                GameObject grappleUI = canvas.transform.GetChild(0).gameObject;
                grappleUI.SetActive(false);
                leftUIActive = false;
            }
            else
            {
                GameObject grappleUI = canvas.transform.GetChild(1).gameObject;
                grappleUI.SetActive(false);
                rightUIActive = false;
            }
        }
    }

    public bool UIGrappleIsActive(GrappleDirection hand)
    {
        if (hand == GrappleDirection.Left)
        {
            return leftUIActive;
        }
        else
        {
            return rightUIActive;
        }
    }

    public void ActivateWallJumpUI()
    {

        if (canvas != null)
        {
            canvas.SetActive(true);
            GameObject wallJumpUI = canvas.transform.GetChild(2).gameObject;
            wallJumpUI.SetActive(true);
            wallJumpActive = true;
        }
    }

    public void DectivateWallJumpUI()
    {
        if (canvas != null)
        {
            if (!leftUIActive && !rightUIActive)
            {
                canvas.SetActive(false);
            }
            GameObject wallJumpUI = canvas.transform.GetChild(2).gameObject;
            wallJumpUI.SetActive(false);
            wallJumpActive = false;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            EventManagerScript.EnableWallJump(this);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            EventManagerScript.DisableWallJump();
        }
    }
}
