using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentScript : MonoBehaviour
{
    public GameObject grappleablePrefab;
    public GameObject groundPrefab;

    public float objectXPosMean = 0;
    public float objectYPosMean = 0;
    public float objectZPosMean = 0;

    public float objectXPosSpread = 0;
    public float objectYPosSpread = 0;
    public float objectZPosSpread = 0;

    public int objectNumber = 1000;
    public int groundWidth = 300;
    public int groundDeapth = -300;

    public float deadRadius = 20;

    // Start is called before the first frame update
    void Start()
    {
        //SpawnGround();
        SpawnGrappleables();
    }

    private void SpawnGround()
    {
        for (int i = 0; i < groundWidth; i++)
        {
            for (int j = 0; j < groundWidth; j++)
            {
                var newPiece = Object.Instantiate(groundPrefab, new Vector3(i, groundDeapth,j), new Quaternion());
                newPiece.transform.parent = gameObject.transform.GetChild(0).transform;
            }
        }
    }

    private void SpawnGrappleables()
    {
        for (int i = 0; i < objectNumber; i++)
        {
            float xPos = Random.Range(-objectXPosSpread, objectXPosSpread)+objectXPosMean;
            float yPos = Random.Range(-objectYPosSpread, objectYPosSpread)+objectYPosMean;
            float zPos = Random.Range(-objectZPosSpread, objectZPosSpread)+objectZPosMean;

            if (Mathf.Sqrt(xPos*xPos+zPos*zPos) > deadRadius)
            {
                var newObject = Object.Instantiate(grappleablePrefab, new Vector3(xPos, yPos, zPos), new Quaternion());
                newObject.transform.parent = gameObject.transform;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            EventManagerScript.ChangeGameState(GameState.Paused);
            EventManagerScript.ChangeUIState(UIState.GameOverMenu);
        }
        else
        {
            Destroy(other.gameObject);
        }
    }
}
