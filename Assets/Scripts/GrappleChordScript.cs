using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleChordScript : MonoBehaviour
{

    public GrappleDirection grappleHand;
    public PlayerGrappleScript playerGrapple;
    public GrappleableObjectScript target;

    public float speed = 0;
    private Vector3 endPosition;
    private float length = 0.0f;

    bool isExtending = false;

    // Start is called before the first frame update
    void Start()
    {
        endPosition = playerGrapple.transform.parent.transform.position;
        isExtending = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(isExtending)
        {
            float distanceTravelledInFrame = Time.deltaTime * speed;
            Vector3 direction = (target.transform.position - playerGrapple.transform.parent.transform.position).normalized;

            length += distanceTravelledInFrame;
            endPosition = length * direction + playerGrapple.transform.parent.transform.position;
            
            if(Vector3.Distance(endPosition,target.transform.position) < 2)
            {
                endPosition = target.transform.position;
                isExtending = false;
                EventManagerScript.SecureGrapple(grappleHand);
            }
        }
        Vector3[] positions = { playerGrapple.transform.parent.transform.position, endPosition };
        gameObject.GetComponent<LineRenderer>().SetPositions(positions);
    }
}
