using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthMeterScript : MonoBehaviour
{
    public Slider healthBar;

    public void UpdateHealth(float newHealth, float maxHealth)
    {
        if(maxHealth >= 0)
        {
            healthBar.value = newHealth / maxHealth;
        }
        else
        {
            healthBar.value = 0.0f;
        }
    }


}
