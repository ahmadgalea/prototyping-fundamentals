using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UIState
{
    StartMenu,
    InGame,
    PauseMenu,
    GameOverMenu,
    VictoryMenu
}


public class UIManagerScript : MonoBehaviour
{

    public static UIState uiState = UIState.StartMenu;

    // Start is called before the first frame update
    void Start()
    {
        EventManagerScript.OnUIStateChange += ChangeUIState;
        ChangeUIState(UIState.StartMenu);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeUIState(UIState newState)
    {
        for(int i=0; i<transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }

        uiState = newState;
        switch (newState)
        {
            case UIState.StartMenu:
                transform.Find("StartMenu").gameObject.SetActive(true);
                break;
            case UIState.InGame:
                transform.Find("InGame").gameObject.SetActive(true);
                break;
            case UIState.PauseMenu:
                transform.Find("PauseMenu").gameObject.SetActive(true);
                break;
            case UIState.GameOverMenu:
                transform.Find("GameOverMenu").gameObject.SetActive(true);
                break;
            case UIState.VictoryMenu:
                transform.Find("VictoryMenu").gameObject.SetActive(true);
                break;
            default:
                break;
        }
    }
}
